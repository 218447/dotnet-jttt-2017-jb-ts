﻿namespace WindowsFormsApp1
{
    partial class ImageView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.description = new System.Windows.Forms.Label();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.image = new System.Windows.Forms.Label();
            this.imageUrl = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // description
            // 
            this.description.AutoSize = true;
            this.description.Location = new System.Drawing.Point(28, 13);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(31, 13);
            this.description.TabIndex = 0;
            this.description.Text = "Opis:";
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(31, 54);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(35, 13);
            this.descriptionLabel.TabIndex = 1;
            this.descriptionLabel.Text = "label2";
            // 
            // image
            // 
            this.image.AutoSize = true;
            this.image.Location = new System.Drawing.Point(34, 112);
            this.image.Name = "image";
            this.image.Size = new System.Drawing.Size(58, 13);
            this.image.TabIndex = 2;
            this.image.Text = "Załącznik:";
            // 
            // imageUrl
            // 
            this.imageUrl.AutoSize = true;
            this.imageUrl.Location = new System.Drawing.Point(34, 171);
            this.imageUrl.Name = "imageUrl";
            this.imageUrl.Size = new System.Drawing.Size(18, 13);
            this.imageUrl.TabIndex = 3;
            this.imageUrl.TabStop = true;
            this.imageUrl.Text = "url";
            // 
            // ImageView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.imageUrl);
            this.Controls.Add(this.image);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.description);
            this.Name = "ImageView";
            this.Text = "ImageView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label description;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Label image;
        private System.Windows.Forms.LinkLabel imageUrl;
    }
}