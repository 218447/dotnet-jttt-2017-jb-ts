﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    

    public partial class WeatherForm : Form
    {
        public WeatherForm()
        {
            InitializeComponent();
            label1.Text = "Miasto: ";
            button1.Text = "Jaka jest pogoda?";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cityNotEntered();
            cityEntered();
        }

        private void cityEntered()
        {
            if (!textBox1.Text.Equals(""))
            {
                
                string weburl = "http://api.openweathermap.org/data/2.5/weather?q=" + textBox1.Text + "&APPID=0574d99316fff8a25734b4cddfd6cb1d";

                API api;
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString(weburl);
                    api = JsonConvert.DeserializeObject<API>(json);
                }
               

                string key = "";

                richTextBox1.Text = "Dzisiaj jest " + api.main.temp.ToString() + " stopni farenheita, " +
                    "ciśnienie " + api.main.pressure.ToString() + " hPa " + "A co na niebie? " + api.weather[0].description ;

              
                string iconurl = "http://openweathermap.org/img/w/" + api.weather[0].icon + ".png";
                pictureBox1.Load(iconurl);
            }
        }

        private void cityNotEntered()
        {
            if (textBox1.Text.Equals(""))
            {
                richTextBox1.Text = "Wpisz miasto";
            }
        }
    }
}
