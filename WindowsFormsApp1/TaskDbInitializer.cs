﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    /// <summary>
    /// Specjalna klasa, któa ma pomagać w inicjalizowaniu bazy danych.
    /// Może dziedziczyć po kilku klasach - jeżeli dziedziczy po DropCreateDatabaseIfModelChanges, 
    /// to baza zostanie przebudowana przy każdej zmianie modelu.
    /// Jezeli dziedziczy po DropCreateDatabaseAlways to przy każdym uruchomieniu baza danych tworzona jest od nowa.
    /// </summary>
    public class TaskDbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<TaskDbContext>
    {

        /// <summary>
        /// Specjalna metoda, która jest wywoływana raz po przebudowaniu bazy danych.
        /// Założenie jest, że baza jest pusta, więc trzeba ją wypełnić początkowymi danymi.
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(TaskDbContext context)
        {
            /*Task task1 = new PageTask("demotywatory.pl", "tak", "tyberio21@gmail.com", "AA");
            Task task2 = new PageTask("kwejk.pl", "nowy", "saknd@gmail.com", "A");
            context.Task.Add(task1);
            context.Task.Add(task2);
            */
            context.SaveChanges();
            base.Seed(context);
        }
    }

}
