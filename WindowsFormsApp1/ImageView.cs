﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class ImageView : Form
    {
        public ImageView(String description, String image)
        {
            InitializeComponent();
            this.description.Text = description;
            imageUrl.Text = image;
        }   
    }
}