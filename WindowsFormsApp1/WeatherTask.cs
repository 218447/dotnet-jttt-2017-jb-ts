﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class WeatherTask : Task
    {
        
        public Int32 Temperature { get; set; }
        public String City { get; set; }

        public WeatherTask(string City, Int32 Temperature, string Mail, string Name)
        {
            this.City = City;
            this.Temperature = Temperature;
            this.Mail = Mail;
            this.Name = Name;
        }
        
        public override String ToString()
        {
            return "Wyślij pogodę na adres:" + Mail + " z miasta " + City + " powyżej " + Temperature;
        }
    }
}