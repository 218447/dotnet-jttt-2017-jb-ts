﻿using System;
using System.Text;
using System.Net;
using System.Net.Mail;

namespace WindowsFormsApp1
{
    public class TaskHandler
    {
        public int ifWordIsPresentThenSend(Task task)
        {
            PageTask pageTask = (PageTask)task;
            string obrazek = searchForAWord(task);
            if (!obrazek.Equals(""))
            {
                var message = new MailMessage();
                message.From = new MailAddress("dotnetjtttbs@gmail.com");
                message.To.Add(new MailAddress(task.Mail));
                message.Subject = "Obrazek na temat: " + pageTask.Word;
                message.Body = obrazek;

                var smtp = new SmtpClient("smtp.gmail.com");
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("dotnetjtttbs@gmail.com", "dotnetjttt");
                smtp.EnableSsl = true;
                smtp.Port = 587;

                smtp.Send(message);
                return 1;
            }
            return 0;
        }

        private string searchForAWord(Task task)
        {
            PageTask pageTask = (PageTask)task;

            var doc = new HtmlAgilityPack.HtmlDocument();
            var pageHtml = GetPageHtml(task);
            doc.LoadHtml(pageHtml);
            var nodes = doc.DocumentNode.Descendants("img");

            String obrazek = "";
           
            foreach (var node in nodes)
            {
                if (node.GetAttributeValue("alt", "").Contains(pageTask.Word))
                {
                    obrazek = node.GetAttributeValue("src", "");
                    break;
                }
            }

            return obrazek;
        }
        private string GetPageHtml(Task task)
        {
            PageTask pageTask = (PageTask)task;

            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode
                    (wc.DownloadString("http://" + pageTask.Page));

                return html;
            }
        }
        public void ShowImage (Task task)
        {
            PageTask pageTask = (PageTask)task;
            ImageView imageView = new ImageView(pageTask.Name, searchForAWord(pageTask));
        }
    }
}
