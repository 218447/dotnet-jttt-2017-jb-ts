﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class TaskDbContext : DbContext
    {
        // Aby podac wlasna nazwe bazy danych, nalezy wywolac konstruktor bazowy z nazwą jako parametrem.
        public TaskDbContext()
            : base("master2")
        {
            // Użyj klasy StudiaDbInitializer do zainicjalizowania bazy danych.
            Database.SetInitializer(new TaskDbInitializer());
        }

        public DbSet<PageTask> PageTasks { get; set; }
        public DbSet<WeatherTask> WeatherTasks { get; set; }
    }

}
