﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;

namespace WindowsFormsApp1
{
    public partial class TaskManager : Form
    {
        private Logger logger = new Logger();
        private TaskHandler taskHandler = new TaskHandler();

        public TaskManager()
        {
            using (var ctx = new TaskDbContext())
            {
                InitializeComponent();

                pageLabel.Text = "Podaj strone do wyszukania na niej obrazka:";
                wordLabel.Text = "Podaj wyraz do wyszukania na podanej stronie:";
                label2.Text = "Podaj adres e-mail do wysłania obrazka:";
                addTask.Text = "Dodaj";
                button2.Text = "Wykonaj";
                button3.Text = "Czyść";
                button4.Text = "Serializuj";
                button5.Text = "Deserializuj";
                /*
                foreach (var g in ctx.Task)
                {
                    listBox1.Items.Add(g);
                }
                */
                logger.Log("zainicjalizowano pola tekstowe");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var ctx = new TaskDbContext())
            { 
                logger.Log("naciśnięto przycisk Dodaj");

                if (checkIfMailIsPresent() && checkIfPageIsPresent() && checkIfWordIsPresent())
                {
                    //sprawdzić które zadanie jest wykonywane i odpowiednio pokierować
                    PageTask task = new PageTask(pageBox.Text, wordBox.Text, mailBox.Text, taskBox.Text);
                    listBox1.Items.Add(task);
                    
                    ctx.PageTasks.Add(task);
                    ctx.SaveChanges();
                    logger.Log("dodano zadanie do listy");
                    label3.Text = "dodano zadanie do listy";
                }
                if (!cityBox.Text.Equals("") && !temperatureBox.Text.Equals("") && !mailBox.Text.Equals(""))
                {
                    WeatherTask task = new WeatherTask(cityBox.Text, Int32.Parse(temperatureBox.Text), mailBox.Text, taskBox.Text);
                    listBox1.Items.Add(task);

                    ctx.WeatherTasks.Add(task);
                    ctx.SaveChanges();
                    logger.Log("dodano zadanie do listy");
                    label3.Text = "dodano zadanie do listy";
                }
                if (!cityBox.Text.Equals("") && !temperatureBox.Text.Equals(""))
                {
                    WeatherTask task = new WeatherTask(cityBox.Text, Int32.Parse(temperatureBox.Text), "no mail", taskBox.Text);
                    listBox1.Items.Add(task);

                    ctx.WeatherTasks.Add(task);
                    ctx.SaveChanges();
                    logger.Log("dodano zadanie do listy");
                    label3.Text = "dodano zadanie do listy";
                }

            }
        }

        private Boolean checkIfPageIsPresent()
        {
            if (pageBox.Text.Equals(""))
            {
                logger.Log("nie podano strony");
                label3.Text = "Wpisz stronę do znalezienia obrazka";
                logger.Log("brak strony, nie dodano zadania");

                return false;
            }
            return true;
        }
        private Boolean checkIfMailIsPresent()
        {
            if (mailBox.Text.Equals(""))
            {
                logger.Log("nie podano maila");
                label3.Text = "Wpisz mail do wysłania obrazka";
                logger.Log("brak maila, nie dodano zadania");

                return false;
            }
            return true;
        }

        private Boolean checkIfWordIsPresent()
        {
            if (wordBox.Text.Equals(""))
            {
                logger.Log("nie podano słowa do wyszukania");
                label3.Text = "Wpisz słowo do wyszukania";
                logger.Log("brak słowa do wyszukania, nie dodano zadania");

                return false;
            }
            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int allItems = listBox1.Items.Count;
            int sentItems = 0;

            logger.Log("naciśnięto przycisk Wykonaj");
            logger.Log("zadań do wykonania: " + allItems);

            foreach(var item in listBox1.Items)
            {
                Task task = (Task)item;
                if (task.Action == Action.comunicat)
                {
                    WeatherTask weatherTask = (WeatherTask)task;
                    ImageView imageView = new ImageView(task.Name, weatherTask.Temperature.ToString());
                }
                sentItems += taskHandler.ifWordIsPresentThenSend(task);
                logger.Log("zadanie wykonano");
            }

            label3.Text = "Znaleziono: " + sentItems + " na " + allItems + " zapytań.";
            logger.Log("Wysłano: " + sentItems + " maili");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            logger.Log("naciśnięto przycisk Czyść");
            listBox1.Items.Clear();
            logger.Log("wyczyszczono listę zadań");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //serializacja
            if (label3.Text != "dodano zadanie do listy")
            {
                label3.Text = "PUSTA LISTA";
            }
            else
            {
                StreamWriter wr = new StreamWriter("dane.xml"); //To służy do zapisu danych
                XmlSerializer serializer = new XmlSerializer(typeof(tymczasowa)); //To będzie je formatowało 
                /*
                foreach (var item in listBox1.Items)
                {
                    Task task = (Task)item;
                    tymczasowa tmp = new tymczasowa(); //musimy miec klase bez parametrow 
                    tmp.page = task.Page;
                    tmp.mail = task.getMail();
                    tmp.word = task.getWord();
                    serializer.Serialize(wr, tmp); //Serializujemy
                }
                */
                label3.Text = "dokonano serializacji";
                wr.Flush();
                wr.Close(); //Sprzątamy
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //deserializacja
            XmlSerializer serializer = new XmlSerializer(typeof(tymczasowa));
            FileStream fs = new FileStream("dane.xml", FileMode.Open);
            /*
            while (fs.Position != fs.Length)
            {
                tymczasowa tmp = (tymczasowa)serializer.Deserialize(fs);
                Task task = new Task(tmp.page, tmp.word, tmp.mail);
                listBox1.Items.Add(task);
            }
            */
            fs.Close();
            label3.Text = "dokonano deserializacji";
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}