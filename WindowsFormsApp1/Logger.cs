﻿using System;
using System.IO;

namespace WindowsFormsApp1
{
    public class Logger
    {
        private StreamWriter streamWriter;

        public Logger()
        {
            File.Delete("jttt.log");
            streamWriter = File.AppendText("jttt.log");
        }

        public void Log(string logMessage)
        {
            streamWriter.Write("\r\nLog Entry : ");
            streamWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            streamWriter.WriteLine("  :");
            streamWriter.WriteLine("  :{0}", logMessage);
            streamWriter.WriteLine("-------------------------------");
            streamWriter.Flush();
        }
    }
}
