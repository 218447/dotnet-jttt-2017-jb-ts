﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class PageTask : Task
    {

        public String Page { get; set; }
        public String Word { get; set; }
       
        public PageTask(string Page, string Word, string Mail, string Name)
        {
            this.Page = Page;
            this.Word = Word;
            this.Mail = Mail;
            this.Name = Name;
        }
        
        public override String ToString()
        {
            return "Znajdź obrazek ze słowem: " + Word + " na stronie: " + Page + " i wyślij na mail: " + Mail;
        }
    }
}
