﻿using System;

namespace WindowsFormsApp1
{
    public class Task
    {
        public int Id { get; set; }
        
        public String Name { get; set; }

        public String Mail { get; set; }

        public Action Action { get; set; }

        public Task()
        {

        }

    }
}